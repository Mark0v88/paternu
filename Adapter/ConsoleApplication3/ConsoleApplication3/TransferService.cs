﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class TransferService
    {
        public void Transfer(ITransport transport)
        {
            transport.CalculateRoute();
        }
    }
}
