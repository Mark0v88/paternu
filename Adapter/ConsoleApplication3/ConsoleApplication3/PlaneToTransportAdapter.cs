﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class PlaneToTransportAdapter : ITransport
    {
        Plane plane;
        public PlaneToTransportAdapter(Plane p, string from, string destination, double distance)
        {
            plane = p;
            plane.From = from;
            plane.Destination = destination;
            plane.Distance = distance;
        }

        public void CalculateRoute()
        {
            plane.CalculateAirTransfer();
        }
    }
}
