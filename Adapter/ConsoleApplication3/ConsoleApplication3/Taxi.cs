﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Taxi : ITransport
    {
        private string From;
        private string Destination;
        private double Distance;
        private double Cost;
        public Taxi(string from, string destination, double distance)
        {
            From = from;
            Destination = destination;
            Distance = distance;
        }
        public void CalculateRoute()
        {
            this.Cost = this.Distance * 7.5;
            Console.WriteLine("Направление: " + this.From + " - " + this.Destination + "\nСтоимость: " + this.Cost);
        }
    }
}
