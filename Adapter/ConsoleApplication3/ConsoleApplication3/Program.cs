﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            TransferService service = new TransferService();
            Taxi taxi = new Taxi("Kiev", "Borispol Airport", 40);
            service.Transfer(taxi);
            Plane plane = new Plane();
            ITransport planeTransport = new PlaneToTransportAdapter(plane, "Airport Borispol","Airport Schiphol", 1000);
            service.Transfer(planeTransport);
            Console.Read();
        }
    }
}
