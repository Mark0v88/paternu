﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Plane : IAirTransport
    {
        public string From;
        public string Destination;
        public double Distance;
        public double Cost;

        public void CalculateAirTransfer()
        {
            this.Cost = this.Distance * 7.3;
            Console.WriteLine("Направление: " + this.From + " - " + this.Destination + "\nСтоимость: " + this.Cost);
        }
    }
}
