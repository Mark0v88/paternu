﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class PageContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
    }
}
