﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            using (IDriver driver = new OrdersDBProxy())
            {
                Order driver1 = driver.GetDriverOrders(1);
                Console.WriteLine(driver1.Text);
                Order driver2 = driver.GetDriverOrders(2);
                Console.WriteLine(driver2.Text);
                Order driver3 = driver.GetDriverOrders(3);
                Console.WriteLine(driver3.Text);
            }

            Console.Read();
        }
    }
}
