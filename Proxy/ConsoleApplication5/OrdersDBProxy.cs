﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class OrdersDBProxy : IDriver
    {
        List<Order> orders;
        OrdersDB bookStore;
        public OrdersDBProxy()
        {
            orders = new List<Order>();
        }
        public Order GetDriverOrders(int number)
        {
            Order page = (Order)orders.Where(c => c.Id == number).Select(c => new { c.From, c.Destination, c.Cost, c.Date });
            return page;
        }

        public void Dispose()
        {
            if (bookStore != null)
                bookStore.Dispose();
        }
    }
}
