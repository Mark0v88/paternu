﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Order
    {
        public int Id { get; set; }
        public int From { get; set; }
        public string Destination { get; set; }
        public string Cost { get; set; }
        public string Date { get; set; }
    }
}
