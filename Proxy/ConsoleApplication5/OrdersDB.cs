﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class OrdersDB : IDriver
    {
        PageContext db;
        public OrdersDB()
        {
            db = new PageContext();
        }
        public Order GetDriverOrders(int number)
        {
            return (Order)db.Orders.Where(c => c.Id == number).Select(c => new { c.From, c.Destination, c.Cost, c.Date });
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
