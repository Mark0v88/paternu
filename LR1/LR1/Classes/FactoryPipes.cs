﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR1.Classes
{
    class FactoryPipes : AbstractionFactoryMetal
    {
        public override AbstractMaterial CreateMaterial()
        {
            return new Pipe();
        }
    }
}
