﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR1.Classes
{
    class FactoryNails : AbstractionFactoryMetal
    {
        public override AbstractMaterial CreateMaterial()
        {
            return new Nail();
        }
    }
}
