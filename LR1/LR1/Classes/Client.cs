﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR1.Classes
{
    class Client
    {
        private readonly AbstractionFactoryMetal _factory;

        public Client(AbstractionFactoryMetal factory)
        {
            _factory = factory;
        }

        public void GetMaterial()
        {
            _factory.CreateMaterial().GetMaterial();
            
        }       
    }
}
