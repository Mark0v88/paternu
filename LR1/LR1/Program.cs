﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LR1.Classes;

namespace LR1
{
    class Program
    {
        static void Main(string[] args)
        {
            Client nail = new Client(new FactoryNails());
            Client pipe = new Client(new FactoryPipes());
            Console.WriteLine("Что Вы хотите?");
            Console.WriteLine("1-Гвозди");
            Console.WriteLine("2-Трубы");
            int count = Int32.Parse(Console.ReadLine());
            if (count == 1)
            {
                nail.GetMaterial();
            }
            else if (count == 2)
            {
                pipe.GetMaterial();
            }
            else { Console.WriteLine("Введите правильное число!"); }
            Console.WriteLine("Спасибо за выбор!");
            Console.ReadLine();
        }
    }
}
