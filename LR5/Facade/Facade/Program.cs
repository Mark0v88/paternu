﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facade.Classes;

namespace Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product = new Product();
            Сashbox сashbox = new Сashbox();
            Calculation calc = new Calculation();

            VisualStudioFacade ide = new VisualStudioFacade(product, сashbox, calc);

            Programmer programmer = new Programmer();
            programmer.CreateApplication(ide);

            Console.Read();
        }
    }
}
