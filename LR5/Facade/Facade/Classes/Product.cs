﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade.Classes
{
    class Product
    {
        public void CreateCode()
        {
            Console.WriteLine("Выбор товара");
        }
        public void Save()
        {
            Console.WriteLine("Взятие товара");
        }
    }
}
