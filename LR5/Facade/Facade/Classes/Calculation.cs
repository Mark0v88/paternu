﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade.Classes
{
    class Calculation
    {
        public void Execute()
        {
            Console.WriteLine("Расчёт за товар");
        }
        public void Finish()
        {
            Console.WriteLine("Уход с магазина");
        }
    }
}
