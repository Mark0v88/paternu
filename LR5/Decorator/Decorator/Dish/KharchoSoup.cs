﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Dish
{
    class KharchoSoup : Soup
    {
        public KharchoSoup()
               : base("Суп харчо")
        { }
        public override int GetCost()
        {
            return 20;
        }
    }
}
