﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Dish
{
    class PeaSoup : Soup
    {
        public PeaSoup() : base("Суп гороховый")
        { }
        public override int GetCost()
        {
            return 15;
        }
    }
}
