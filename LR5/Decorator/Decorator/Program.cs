﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator.Classes;
using Decorator.Dish;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            Soup soup1 = new PeaSoup();
            soup1 = new SourСream(soup1); // гороховый суп со сметаной
            Console.WriteLine("Название: {0}", soup1.Name);
            Console.WriteLine("Цена: {0}", soup1.GetCost());

            Soup soup2 = new PeaSoup();
            soup2 = new Doughnut(soup2);// гороховый суп с пампушкой
            Console.WriteLine("Название: {0}", soup2.Name);
            Console.WriteLine("Цена: {0}", soup2.GetCost());

            Soup soup3 = new KharchoSoup();
            soup3 = new SourСream(soup3);
            soup3 = new Doughnut(soup3);// суп харчо со сметаной и пампушкой
            Console.WriteLine("Название: {0}", soup3.Name);
            Console.WriteLine("Цена: {0}", soup3.GetCost());

            Console.ReadLine();
        }
    }
}
