﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator.Dish;

namespace Decorator.Classes
{
    class Doughnut : SoupDecorator
    {
        public Doughnut(Soup p)
                : base(p.Name + ", с пампушкой", p)
        { }

        public override int GetCost()
        {
            return pizza.GetCost() + 10;
        }
    }
}
