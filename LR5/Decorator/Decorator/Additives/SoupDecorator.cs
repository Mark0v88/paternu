﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator.Dish;

namespace Decorator.Classes
{
    abstract class SoupDecorator : Soup
    {
        protected Soup pizza;
        public SoupDecorator(string n, Soup pizza) : base(n)
        {
            this.pizza = pizza;
        }
    }
}
