﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator.Dish;

namespace Decorator.Classes
{
    class SourСream : SoupDecorator
    {
        public SourСream(Soup p)
                : base(p.Name + ", с сметаной", p)
        { }

        public override int GetCost()
        {
            return pizza.GetCost() + 5;
        }
    }
}
