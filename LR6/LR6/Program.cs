﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LR6.Command;
using LR6.ChainOfResponsibility;


namespace LR6
{
    class Program
    {
        static void Main(string[] args)
        {
            RawMaterial rawMaterial = new RawMaterial();
            Treatment treatment = new Treatment();
            Marketolog marketolog = new Marketolog();

            List<ICommand> commands = new List<ICommand>
            {
            new CodeCommand(rawMaterial),
            new TestCommand(treatment),
            new AdvertizeCommand(marketolog)
            };
            Manager manager = new Manager();
            manager.SetCommand(new MacroCommand(commands));
            manager.StartProject();
            manager.StopProject();

            Receiver receiver = new Receiver(false, true, true);

            PaymentHandler bankPaymentHandler = new BankPaymentHandler();
            PaymentHandler moneyPaymentHnadler = new MoneyPaymentHandler();
            PaymentHandler paypalPaymentHandler = new PayPalPaymentHandler();
            bankPaymentHandler.Successor = paypalPaymentHandler;
            paypalPaymentHandler.Successor = moneyPaymentHnadler;

            bankPaymentHandler.Handle(receiver);
            Console.Read();
        }
    }
}
