﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR6.Command
{
    class Treatment
    {
        public void StartTest()
        {
            Console.WriteLine("Начало обработки сыръя");
        }
        public void StopTest()
        {
            Console.WriteLine("Прекращение обработки сыръя");
        }
    }
}
