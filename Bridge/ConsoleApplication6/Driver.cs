﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    abstract class Driver
    {
        protected ICarType type;
        public string From;
        public string Destination;
        public double Distance;
        public ICarType Type
        {
            set { type = value; }
        }
        public Driver(ICarType lang, string from, string destination, double distance)
        {
            type = lang;
            From = from;
            Destination = destination;
            Distance = distance;
        }
        public virtual void MakeOrderId()
        {
            type.GenerateOrderId();
        }
        public abstract void CalculateOrder();
    }
}
