﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            Driver driver = new NightDriver(new Car(), "Kiev", "Borispol Airport", 40);
            driver.MakeOrderId();
            driver.CalculateOrder();
            driver.Type = new Truck();
            driver.MakeOrderId();
            driver.CalculateOrder();
            Console.Read();
        }
    }
    
}
