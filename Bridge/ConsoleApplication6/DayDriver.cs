﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class DayDriver : Driver
    {
        public DayDriver(ICarType lang, string from, string destination, double distance)
            : base(lang, from, destination, distance)
        {
        }
        public override void CalculateOrder()
        {
            double cost = Distance * 2.5;
            Console.WriteLine("Направление: " + From + " - " + Destination + "\nСтоимость: " + cost);
        }
    }
}