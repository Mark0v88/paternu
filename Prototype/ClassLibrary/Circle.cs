﻿using Interfaces;
using System;

namespace ClassLibrary
{
    public class Circle : IFigure
    {
        int radius;
        int X;
        int Y;
        public Circle(int r, int x, int y)
        {
            radius = r;
            X = x;
            Y = y;
        }

        public IFigure Clone()
        {
            return new Circle(this.radius, this.X, this.Y);
        }
        public void GetInfo()
        {
            Console.WriteLine("Круг радиусом {0} и центром в точке ({1}, {2})", radius, X, Y);
        }
    }
}
