﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication7
{
    class TransferService
    {
        Dictionary<string, TypeCars> orders = new Dictionary<string, TypeCars>();
        public TransferService()
        {
            orders.Add("Car", new Car());
            orders.Add("Truck", new Truck());
        }

        public TypeCars Transfer(string key)
        {
            if (orders.ContainsKey(key))
                return orders[key];
            else
                return null;
        }
    }
}
