﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication7
{
    class Truck : TypeCars
    {

        public override void CalculateRoute(string from, string destination, double distance)
        {
            double cost = distance * 7.5;
            Console.WriteLine("Направление: " + from + " - " + destination + ", Расстояние: " + distance + ", Стоимость:" + cost);
        }
    }
}
