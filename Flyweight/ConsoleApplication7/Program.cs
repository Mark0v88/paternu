﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication7
{
    class Program
    {
        static void Main(string[] args)
        {
            double distance = 300;
            Random rnd = new Random();
            TransferService service = new TransferService();
            for (int i = 0; i < 5; i++)
            {
                TypeCars car = service.Transfer("Car");
                car.CalculateRoute("Kremenchuk", "Kiev", distance);
                distance += rnd.Next(0, 5);
            }

            for (int i = 0; i < 5; i++)
            {
                TypeCars truck = service.Transfer("Truck");
                truck.CalculateRoute("Kremenchuk", "Kiev", distance);
                distance += rnd.Next(0, 5);
            }

            Console.Read();
        }
    }
}
