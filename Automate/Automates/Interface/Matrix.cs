﻿
using System;

namespace Automates
{
    /// <summary>
    /// Product
    /// </summary>
    abstract class Matrix
    {
        protected int[,] baseMass { get; set; }
        protected int[,] auxiliaryMass { get; set; }

        protected int[] row { get; set; }
        protected int[] auxiliaryRow { get; set; }

        public Matrix(){
            //matrix
            baseMass = new int[23, 23];
            auxiliaryMass = new int[23, 23];
            //matrix

            //row
            row = new int[53];
            row=RowFilling(row);
            auxiliaryRow = new int[53];
            //row
        }

        public int[] RowFilling(int[] array)
        {
            var center = array.Length / 2 - 1;
            for (var i = 0; i < array.Length; i++)
            {
                array[center] = 1;
            }
            return array;
        }

        public int[] TransferValue(int[] auxiliary, int[] baseRow)
        {
            for (var i = 0; i < baseRow.Length - 1; i++)
            {
                if (auxiliary[i] != baseRow[i])
                    baseRow[i] = auxiliary[i];
            }
            return baseRow;
        }

        public void ConsoleArray(int[] array)
        {
            for (var i = 0; i < array.Length - 1; i++)
            {
                if (array[i] == 1)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else Console.ForegroundColor = ConsoleColor.DarkGreen;

                Console.Write(array[i]);
            }
            Console.WriteLine();
        }

        //matrix
        public int[,] TransferValue(int[,] auxiliary, int[,] baseRow)
        {
            for (var i = 0; i < 22; i++)
            {
                for (var j = 0; j < 22; j++)
                {
                        baseMass[i,j] = auxiliaryMass[i,j];
                }
            }
            return baseMass;
        }

        public void ConsoleArray(int[,] array)
        {
            Console.WriteLine();
            for (var i = 0; i < 22; i++)
            {
                for (var j = 0; j < 22; j++)
                {
                    if (array[i, j] == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else Console.ForegroundColor = ConsoleColor.DarkGreen;

                    Console.Write(array[i,j]);
                }
                Console.WriteLine();
            }
        }

        public int[,] MatrixFilling(int[,] array)
        {
            for (var i = 0; i < 22; i++)
            {
                for (var j = 0; j < 22; j++)
                {
                    array[i,j] = 0;
                }
            }
            //рисунок изначальный
            array[10, 10] = 1;
            array[11, 9] = 1;
            array[11, 11] = 1;
            array[12, 10] = 1;
            array[13, 9] = 1;
            array[13, 11] = 1;
            array[14, 10] = 1;
            return array;
        }

        public int[,] MatrixFillingCircle(int[,] array)
        {
            for (var i = 0; i < 22; i++)
            {
                for (var j = 0; j < 22; j++)
                {
                    array[i, j] = 0;
                }
            }
            //рисунок изначальный
            array[10, 10] = 1;
            array[11, 9] = 1;
            array[11, 11] = 1;
            array[12, 10] = 1;
            array[13, 9] = 1;
            array[13, 11] = 1;
            array[14, 10] = 1;
            //array[11,11] = 1;
            //array[10,11] = 1;
            //array[9,11] = 1;
            //array[11,12] = 1;
            //array[10,13] = 1;
            return array;
        }
    }
}
