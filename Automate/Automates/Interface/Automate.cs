﻿
namespace Automates
{
    /// <summary>
    /// Creator
    /// </summary>
    abstract class Automate
    {
        public abstract Matrix AlgorithmGo();
        public abstract void AlgorithmNumber();
    }
}
