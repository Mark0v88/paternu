﻿
using System;

namespace Automates.Algorithms
{
    class SecondAlgorithms : Automate
    {
        public override Matrix AlgorithmGo()
        {
            return new SecondRule();
        }
        public override void AlgorithmNumber()
        {
            Console.WriteLine("Second Algorithm");
        }
    }
}
