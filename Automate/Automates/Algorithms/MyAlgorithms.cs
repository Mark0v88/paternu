﻿using System;

namespace Automates
{
    class MyAlgorithms : Automate
    {
        public override Matrix AlgorithmGo()
        {
            return new MyRule();
        }

        public override void AlgorithmNumber()
        {
            Console.WriteLine("First Algorithm");
        }
    }
}
