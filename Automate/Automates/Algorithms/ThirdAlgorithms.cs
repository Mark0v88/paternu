﻿using System;

namespace Automates.Algorithms
{
    class ThirdAlgorithms : Automate
    {
        public override Matrix AlgorithmGo()
        {
            return new ThirdRule();
        }
        public override void AlgorithmNumber()
        {
            Console.WriteLine("Third Algorithm");
        }
    }
}
