﻿using Automates;
using System;

namespace Automates.Algorithms
{
    class PascalTriangleAlgorithm:Automate
    {
        public override Matrix AlgorithmGo()
        {
            return new PascalTriangleRule();
        }
        public override void AlgorithmNumber()
        {
            Console.WriteLine("PascalTriangle Algorithm");
        }
    }
}
