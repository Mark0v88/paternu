﻿using System;

namespace Automates
    {
        class EightAlgoritm : Automate
        {
            public override Matrix AlgorithmGo()
            {
                return new EightRule();
            }

            public override void AlgorithmNumber()
            {
                Console.WriteLine("matrix Algorithm");
            }
        }
    }
