﻿
using System;

namespace Automates.Algorithms
{
    class FirstAlgorithm : Automate
    {
        public override Matrix AlgorithmGo()
        {
            return new FirstRule();
        }

        public override void AlgorithmNumber()
        {
            Console.WriteLine("First Algorithm");
        }
    }
}
