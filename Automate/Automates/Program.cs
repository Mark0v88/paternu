﻿using Automates.Algorithms;
using System;

namespace Automates
{
    class Program
    {
        static void Main(string[] args)
        {
            Automate go = new FirstAlgorithm();
            Matrix matrix = go.AlgorithmGo();
            Console.WriteLine("");
            go = new SecondAlgorithms();
            matrix = go.AlgorithmGo();
            Console.WriteLine("");
            go = new ThirdAlgorithms();
            matrix = go.AlgorithmGo();
            //Console.WriteLine("                    Моё правело");
            //go = new MyAlgorithms();
            //matrix = go.AlgorithmGo();
            ////go = new PascalTriangleAlgorithm();
            ////matrix = go.AlgorithmGo();
            //Console.WriteLine("Новое  правело");
            //go = new EightAlgoritm();
            //matrix = go.AlgorithmGo();
            Console.ReadKey();
        }
    }
}
