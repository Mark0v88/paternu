﻿

namespace Automates
{
    /// <summary>
    /// Concrete Product
    /// </summary>
    class FirstRule : Matrix
    {
        public FirstRule() : base()
        {
            var n = 0;
            do
            {
                ConsoleArray(row);
                Rule();
                row = TransferValue(auxiliaryRow, row);
                n++;
            }
            while (n != 10);
        }

        // Функция поведения клетки
        int Func(int y1, int y2, int y3)
        {
            return y1 | y2 | y3;
        }

        // Функция, обеспечивающая заворачивание структуры
        public int TorIt(int x)
        {
            if (x < 0) return x + row.Length; else return x % row.Length;
        }

        public void Rule()
        {
            var n = row.Length - 1;
            for (var i = 0; i < n; i++)
            {
                auxiliaryRow[TorIt(i)] = Func(row[TorIt(i - 1)], row[TorIt(i)], row[TorIt(i + 1)]);
            }
        }
    }
}