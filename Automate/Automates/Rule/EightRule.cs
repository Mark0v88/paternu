﻿
namespace Automates
{
    class EightRule : Matrix
    {
        public EightRule() : base()
        {
            var n = 0;
            baseMass = MatrixFillingCircle(baseMass);
            do
            {
                ConsoleArray(baseMass);
                Rule();
                baseMass = TransferValue(auxiliaryMass, baseMass);
                n++;
            }
            while (n != 10);
        }

        // Функция поведения клетки
        // U - Верх; UR - Верх-Право; R - Право; DR - Низ-Право;
        // D - Низ; DL - Низ-Лево; L - Лево; UL - Верх-Лево
        int Func(int y, int yU, int yUR, int yR, int yDR, int yD, int yDL, int
        yL, int yUL)
        {
            // Вычисление количества живых соседей
            int i = yU + yUR + yR + yDR + yD + yDL + yL + yUL;
            // Мертвая клетка оживет, если у нее имеется 3 живых соседа
            if (y == 0 && i == 3) return 1;
            // Живая клетка останется живой, если у нее имеется 2 или 3
            // живых соседа
            if (y == 1 && (i == 2 || i == 3)) return 1;
            // В остальных случаях клетка будет мертвой
            return 0;
        }

        // Функция, обеспечивающая заворачивание структуры
        public int TorIt(int x)
        {
            if (x < 0) return x + 23; else return x % 23;
        }

        public void Rule()
        {
            var n = 22;
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    auxiliaryMass[i, j] = Func(baseMass[TorIt(i), TorIt(j)],
                                               baseMass[TorIt(i - 1), TorIt(j)], baseMass[TorIt(i - 1), TorIt(j + 1)],
                                               baseMass[TorIt(i), TorIt(j + 1)], baseMass[TorIt(i + 1), TorIt(j + 1)],
                                               baseMass[TorIt(i + 1), TorIt(j)], baseMass[TorIt(i + 1), TorIt(j - 1)],
                                               baseMass[TorIt(i), TorIt(j - 1)], baseMass[TorIt(i - 1), TorIt(j - 1)]);
                }
            }
        }
    }
}

