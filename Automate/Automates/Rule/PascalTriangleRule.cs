﻿
namespace Automates
{
    class PascalTriangleRule : Matrix
    {
        public PascalTriangleRule() : base()
        {
            var n = 0;
            baseMass = MatrixFilling(baseMass);
            do
            {
                ConsoleArray(baseMass);
                Rule();
                baseMass = TransferValue(auxiliaryMass, baseMass);
                n++;
            }
            while (n !=9);
        }

        // Функция поведения клетки
        // U - Верх; UR - Верх-Право; R - Право; DR - Низ-Право;
        // D - Низ; DL - Низ-Лево; L - Лево; UL - Верх-Лево
        int Func(int y, int yU, int yUR, int yR, int yDR, int yD, int yDL, int
        yL, int yUL)
        {
            int i = yU ^ yUR ^ yR ^ yDR ^ yD ^ yDL ^ yL ^ yUL; return i;
        }

        // Функция, обеспечивающая заворачивание структуры
        public int TorIt(int x)
        {
            if (x < 0) return x + 23; else return x % 23;
        }

        public void Rule()
        {
            var n = 22;
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    auxiliaryMass[i, j] = Func(baseMass[TorIt(i), TorIt(j)],
                                               baseMass[TorIt(i - 1), TorIt(j)], baseMass[TorIt(i - 1), TorIt(j + 1)],
                                               baseMass[TorIt(i), TorIt(j + 1)], baseMass[TorIt(i + 1), TorIt(j + 1)],
                                               baseMass[TorIt(i + 1), TorIt(j)], baseMass[TorIt(i + 1), TorIt(j - 1)],
                                               baseMass[TorIt(i), TorIt(j - 1)], baseMass[TorIt(i - 1), TorIt(j - 1)]);
                }
            }
        }
    }
}

