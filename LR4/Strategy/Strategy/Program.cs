﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Classes;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            int count;
            string model;
            int move;
            Car auto;
            do
            {
                Console.WriteLine("Количество пассажиров");
                count = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Марка");
                model = Convert.ToString(Console.ReadLine());
                Console.WriteLine("1 - Перемещение на электричестве\n2 - Перемещение на бензине");
                move = Convert.ToInt32(Console.ReadLine());
                if (move == 1)
                {
                    auto = new Car(count, model, new ElectricMove());
                    auto.Move();
                }
                else if (move == 2)
                {
                    auto = new Car(count, model, new PetrolMove ());
                    auto.Move();
                }
            } while (true);
        }
    
    }
}
