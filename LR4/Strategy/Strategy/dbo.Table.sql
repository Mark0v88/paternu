﻿CREATE TABLE [dbo].[Registration]
(
	[Id] INT NOT NULL IDENTITY , 
    [Last_Name] NCHAR(25) NOT NULL, 
    [Firstr_Name] NCHAR(15) NOT NULL, 
    [Date_of_Brith] DATE NOT NULL, 
    [City] NCHAR(25) NOT NULL, 
    [Street] NCHAR(30) NOT NULL, 
    [House] INT NOT NULL, 
    [Apartament] INT NOT NULL, 
    CONSTRAINT [PK_Registration] PRIMARY KEY ([Id])
)
