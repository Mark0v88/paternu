﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
            Component fileSystem = new Directory("Transfer Service File System");
            Component diskC = new Directory("Disk С");
            Component driverFile = new File("DriverList.docx");
            Component carsFile = new File("CarsList.xls");
            Component taxiFile = new File("TaxiList.xls");
            diskC.Add(driverFile);
            diskC.Add(carsFile);
            diskC.Add(taxiFile);
            fileSystem.Add(diskC);
            fileSystem.Print();
            Console.WriteLine();
            diskC.Remove(taxiFile);
            Component docsFolder = new Directory("Transfer Documents");
            Component bankFile = new File("BankTrasactions.xls");
            Component contractFile = new File("DriverContract.docx");
            docsFolder.Add(bankFile);
            docsFolder.Add(contractFile);
            diskC.Add(docsFolder);
            fileSystem.Print();

            Console.Read();
        }
    }
}
