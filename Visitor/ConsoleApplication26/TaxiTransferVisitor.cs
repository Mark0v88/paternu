﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication26
{
    class TaxiTransferVisitor : IVisitor
    {
        public void StandartTariff(StandartClient client)
        {
            double cost = client.Distance * 5.5;
            Console.WriteLine("Направление: " + client.From + " - " + client.Destination + "\nТип трансорта:Автомобиль\nКласс:Стандарт\nСтоимость: " + cost);
        }

        public void BusinessTariff(BusinessClient client)
        {
            double cost = client.Distance * 10;
            Console.WriteLine("Направление: " + client.From + " - " + client.Destination + "\nТип трансорта:Автомобиль\nКласс:Бизнес\nСтоимость: " + cost);
        }
    }
}
