﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication26
{
    interface IVisitor
    {
        void StandartTariff(StandartClient client);
        void BusinessTariff(BusinessClient client);
    }
}
