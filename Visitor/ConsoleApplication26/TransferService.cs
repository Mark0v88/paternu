﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication26
{
    class TransferService
    {
        List<IClient> clients_data = new List<IClient>();
        public void Add(IClient acc)
        {
            clients_data.Add(acc);
        }
        public void Remove(IClient acc)
        {
            clients_data.Remove(acc);
        }
        public void Accept(IVisitor visitor)
        {
            foreach (IClient acc in clients_data)
                acc.Accept(visitor);
        }
    }
}
