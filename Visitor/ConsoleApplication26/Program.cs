﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication26
{
    class Program
    {
        static void Main(string[] args)
        {
            var structure = new TransferService();
            structure.Add(new StandartClient { From = "Soborna 23", Destination = "Teatlana 11", Distance = 3.5});
            structure.Add(new BusinessClient { From = "Soborna 23", Destination = "Teatlana 11", Distance = 3.5, CardNumber = "F45673" });
            structure.Accept(new TaxiTransferVisitor());
            structure.Accept(new BusTransferVisitor());

            Console.Read();
        }
    }

   
}
