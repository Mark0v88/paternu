﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication26
{
    class BusTransferVisitor : IVisitor
    {
        public void StandartTariff(StandartClient client)
        {
            double cost = client.Distance * 3;
            Console.WriteLine("Направление: " + client.From + " - " + client.Destination + "\nТип трансорта:Автобус\nКласс:Стандарт\nСтоимость: " + cost);
        }

        public void BusinessTariff(BusinessClient client)
        {
            double cost = client.Distance * 6;
            Console.WriteLine("Направление: " + client.From + " - " + client.Destination + "\nТип трансорта:Автобус\nКласс:Бизнес\nСтоимость: " + cost);
        }
    }
}
