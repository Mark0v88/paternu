﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication26
{
    interface IClient
    {
        void Accept(IVisitor visitor);
    }
}
