﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication26
{
    class BusinessClient : IClient
    {
        public string From { get; set; }
        public string Destination { get; set; }
        public double Distance { get; set; }
        public string CardNumber { get; set; }

        public void Accept(IVisitor visitor)
        {
            visitor.BusinessTariff(this);
        }
    }
}
