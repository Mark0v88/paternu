﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Client
{
    public class Clients
    {
        Console.WriteLine("What events do you want to know ?\n" +
                "1 - Technology Event\n" +
                "2 - Football Event\n" +
                "3 - Party Event");
            int count = Int32.Parse(Console.ReadLine());
        IKernel ninjectKernel = new StandardKernel(new SimpleConfigModule(count));
        Client college = ninjectKernel.Get<College>();
        college.GetEvents();
            Console.ReadLine();
    }
}
