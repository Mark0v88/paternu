﻿using Function;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Technology.Tool
{
    public class FactoryTechnology : AbstractFactoryEvent
    {
        public override AbstractEvent CreateEvent()
        {
            return new Technology();
        }
    }
}
