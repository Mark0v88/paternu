﻿using System;
using Ninject;
using Configuration;
using Function;

namespace LR2
{
    class Program
    {
        //interface IEvent
        //{
        //    void LoadEventDetail();
        //}
        
        //class TechEvent : IEvent
        //{
        //    public void LoadEventDetail()
        //    {
        //        Console.WriteLine("Technology Event Details");
        //    }
        //}
        //class FootballEvent : IEvent
        //{
        //    public void LoadEventDetail()
        //    {
        //        Console.WriteLine("Football Event Details");
        //    }
        //}
        //class PartyEvent : IEvent
        //{
        //    public void LoadEventDetail()
        //    {
        //        Console.WriteLine("Party Event Details");
        //    }
        //}
        //class College
        //{
        //    private IEvent _events = null;
        //    public College(IEvent ie)
        //    {
        //        this._events = ie;
        //    }
        //    public void GetEvents()
        //    {
        //        this._events.LoadEventDetail();
        //    }
        //}
        //class SimpleConfigModule : NinjectModule
        //{
        //    private int x;
        //    public SimpleConfigModule(int e)
        //    {
        //        this.x = e;    
        //    }
        //    public override void Load()
        //    {
        //        if (x == 1)
        //        {
        //            Bind<IEvent>().To<TechEvent>();
        //        }
        //        else if(x == 2)
        //        {
        //            Bind<IEvent>().To<FootballEvent>();
        //        }
        //        else if(x == 3)
        //        {
        //            Bind<IEvent>().To<PartyEvent>();
        //        }
        //        Bind<College>().ToSelf();
        //    }
        //}
        static void Main(string[] args)
        {
            Console.WriteLine("What events do you want to know ?\n" +
                "1 - Technology Event\n" +
                "2 - Football Event\n" +
                "3 - Party Event");
            int count = Int32.Parse(Console.ReadLine());
            IKernel ninjectKernel = new StandardKernel(new SimpleConfigModule(count));
            Client college = ninjectKernel.Get<Client>();
            college.GetEvent();
            Console.ReadLine();
        }
    }
}
