﻿using Function;

namespace FootballEvent.Tool
{
    public class FactoryFootball : AbstractFactoryEvent
    {
        public override AbstractEvent CreateEvent()
        {
            return new Football();
        }
    }
}
