﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public abstract class AbstractFactoryEvent
    {
        public abstract AbstractEvent CreateEvent();
    }
}
