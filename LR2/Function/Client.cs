﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class Client
    {
        private readonly AbstractFactoryEvent _factory;

        public Client(AbstractFactoryEvent factory)
        {
            _factory = factory;
        }

        public void GetEvent()
        {
            _factory.CreateEvent().GetEvent();

        }
    }
}
