﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{

    abstract public class AbstractEvent
    {
        public abstract void GetEvent();
    }
}
