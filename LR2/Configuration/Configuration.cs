﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using Function;
using FootballEvent.Tool;
using PartyEvent.Tool;
using Technology.Tool;

namespace Configuration
{
    public class SimpleConfigModule : NinjectModule
    {
        private int x;
        public SimpleConfigModule(int e)
        {
            this.x = e;
        }
        public override void Load()
        {
            if (x == 1)
            {
                Bind<AbstractFactoryEvent>().To<FactoryTechnology>();
            }
            else if (x == 2)
            {
                Bind<AbstractFactoryEvent>().To<FactoryFootball>();
            }
            else if (x == 3)
            {
                Bind<AbstractFactoryEvent>().To<FactoryParty>();
            }
            Bind<Client>().ToSelf();
        }
    }
}
