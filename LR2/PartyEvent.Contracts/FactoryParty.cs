﻿using Function;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartyEvent.Tool
{
    public class FactoryParty : AbstractFactoryEvent
    {
        public override AbstractEvent CreateEvent()
        {
            return new Party();
        }
    }
}
