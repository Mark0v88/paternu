﻿namespace Intrrfaces
{
    public interface IWaterState
    {
        void Heat(Water water);     
        void Frost(Water water);
    }
}
