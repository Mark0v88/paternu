﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpeter.Classes
{
    interface IExpression
    {
        int Interpret(Context context);
    }
}
